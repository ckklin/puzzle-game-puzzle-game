package Game;

public class User {
    private String username;
    private String Password;
    public User(){
    }
    public User(String username,String Password){
        this.username=username;
        this.Password=Password;
    }
    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username=username;
    }
    public String getPassword(){
        return Password;
    }
    public void setPassword(String Password){
        this.Password=Password;

    }
}
