package Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class RegisterJFrame extends JFrame implements ActionListener {
//    LoginJFrame login=new LoginJFrame();

    static ArrayList<User> list0=new ArrayList<>();
    static{
//        ArrayList<User> list=new ArrayList<>();
        User leader=new User("钞人","001");
        list0.add(leader);
    }
    JTextField inputName=new JTextField();
    JTextField inputPassword=new JTextField();
    JTextField againPassword=new JTextField();

    //注册界面
    public RegisterJFrame(){
       initJFrame();

        initView();

       initImage();

        //将窗体显示出来，一般写在最后面
        this.setVisible(true);
    }
    private void initJFrame(){
        this.setSize(488,430);
        this.setTitle("用户注册");
        //将窗体始终显示于最表层
        this.setAlwaysOnTop(true);
        //窗体居中
        this.setLocationRelativeTo(null);
        //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
        this.setLayout(null);
        //设置模式
        this.setDefaultCloseOperation(3);
    }
    private void initImage(){
        JLabel jLabel=new JLabel(new ImageIcon("image\\wq.png"));
        jLabel.setBounds(0,0,488,430);
        this.getContentPane().add(jLabel);
    }

    private void initView(){
        //标题
        JLabel first=new JLabel("欢迎你加入我们");
        first.setForeground(Color.yellow);
        Font font0=new Font("微软雅黑",1,39);
        first.setFont(font0);
        first.setBounds(116,20,311,77);
        this.getContentPane().add(first);

        //注册用户名
        JLabel name=new JLabel("用户名");
        name.setForeground(Color.red);
        Font font1=new Font("微软雅黑",1,15);
        name.setFont(font1);
        name.setBounds(142,120,66,22);
        this.getContentPane().add(name);


        inputName.setBounds(203,120,133,30);
        this.getContentPane().add(inputName);

        //注册密码
        JLabel password=new JLabel("密码");
        password.setForeground(Color.red);
        Font font2=new Font("微软雅黑",1,15);
        password.setFont(font2);
        password.setBounds(156,180,133,30);
        this.getContentPane().add(password);


        inputPassword.setBounds(203,180,133,30);
        this.getContentPane().add(inputPassword);

        JLabel password1=new JLabel("确认密码");
        password1.setForeground(Color.red);
        Font font3=new Font("微软雅黑",1,15);
        password1.setFont(font3);
        password1.setBounds(126,240,66,22);
        this.getContentPane().add(password1);


        againPassword.setBounds(203,240,133,30);
        this.getContentPane().add(againPassword);

        //添加按钮
        JButton jb=new JButton("注册");
        jb.setForeground(Color.black);
        Font font4=new Font("微软雅黑",0,16);
        jb.setFont(font4);
        jb.setBounds(203,311,66,30);
        this.getContentPane().add(jb);


        //将注册按钮绑定鼠标监听事件
        jb.addActionListener(this);
    }
   /* public void checkName(){
        String newName=inputName.getText();
        String newPassword=inputPassword.getText();
        User user1=new User(newName,newPassword);
        int j=2;
        for(int i=0;i<list0.size();i++){
            User user=list0.get(i);
            if(user1.getUsername().equals(user.getUsername())) {
                j = 1;
            }else if(newName.equals("")) {
                j = 0;
            }
        }

        if(j==1) {
            JDialog jb = new JDialog();

            JLabel JName = new JLabel("该用户名已被使用，请换一个吧");
            JName.setBounds(22, 1, 191, 25);
            jb.getContentPane().add(JName);
            jb.setSize(282, 66);
            //让弹框置顶
            jb.setAlwaysOnTop(true);
            //让弹框居中
            jb.setLocationRelativeTo(null);
            //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
            jb.setLayout(null);
            //弹框不关闭则无法进行下面的界面
            jb.setModal(true);
            //让弹框显示出来
            jb.setVisible(true);
        }

        //判断用户名是否为空
        if(j==0){
            JDialog jb=new JDialog();

            JLabel JName=new JLabel("请输入你的用户名");
            JName.setBounds(22,1,191,25);
            jb.getContentPane().add(JName);
            jb.setSize(282,66);
            //让弹框置顶
            jb.setAlwaysOnTop(true);
            //让弹框居中
            jb.setLocationRelativeTo(null);
            //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
            jb.setLayout(null);
            //弹框不关闭则无法进行下面的界面
            jb.setModal(true);
            //让弹框显示出来
            jb.setVisible(true);
        }

        if(j==2) {
            checkPassword();
        }
    }
    public void checkPassword(){
        String password1=inputPassword.getText();
        String password2=againPassword.getText();
        String newname=inputName.getText();
        if(!password1.equals(password2)){
            JDialog jPassword=new JDialog();
            JLabel jl=new JLabel("两次密码输入不一致，请重新输入");
            jl.setBounds(22,1,191,25);
            jPassword.getContentPane().add(jl);

            jPassword.setSize(282,66);
            jPassword.setAlwaysOnTop(true);
            jPassword.setLocationRelativeTo(null);
            jPassword.setModal(true);
            //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
            jPassword.setLayout(null);
            jPassword.setVisible(true);

        }else if(password1.equals("")){
            JDialog jPassword=new JDialog();
            JLabel jl=new JLabel("请输入你的密码");
            jl.setBounds(22,1,191,25);
            jPassword.getContentPane().add(jl);

            jPassword.setSize(282,66);
            jPassword.setAlwaysOnTop(true);
            jPassword.setLocationRelativeTo(null);
            jPassword.setModal(true);
            //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
            jPassword.setLayout(null);
            jPassword.setVisible(true);
        }else{
            User newUser=new User(password1,newname);
            list0.add(newUser);
            this.setVisible(false);
            /////////////////////////////////////////////
            LoginJFrame.getInstance().setVisible(true);
        }

    }*/
    public void checkInput(){
        String newName=inputName.getText();
        String password1=inputPassword.getText();
        String password2=againPassword.getText();
        User user=new User(newName,password1);
        //先查找用户名相同的用户，找到就提示并退出
        //List <User> matchUser = list0.stream().filter(s->s.getUsername().equals(newName)).collect(Collectors.toList());
        for (int i = 0; i < list0.size(); i++) {
            if (list0.get(i).getUsername().equals(newName)) {
                createDialogMessage("该用户名已被使用，请换一个吧");
                return;
            }
        }
        if(newName.equals("")){
            createDialogMessage("请输入用户名");
        }else if(password1.equals("")){
            createDialogMessage("请输入密码");
        }else if(!password2.equals(password1)){
            createDialogMessage("两次密码输入不一致，请重新输入");
        }else{
            list0.add(user);
            this.setVisible(false);
            LoginJFrame.getInstance().setVisible(true);
        }
    }
    void createDialogMessage(String message){
        JDialog jb=new JDialog();
        JLabel JName=new JLabel(message);
        JName.setBounds(22,1,191,25);
        jb.getContentPane().add(JName);
        jb.setSize(282,66);
        //让弹框置顶
        jb.setAlwaysOnTop(true);
        //让弹框居中
        jb.setLocationRelativeTo(null);
        //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
        jb.setLayout(null);
        //弹框不关闭则无法进行下面的界面
        jb.setModal(true);
        //让弹框显示出来
        jb.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
       // checkName();
        checkInput();
    }
}
