package Game;


import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class GameJFrame extends JFrame implements KeyListener, ActionListener {
    //游戏主界面

    //创建一个二维数组
    //目的：用来管理数据
    //加载图片的时候，根据二维数组中的数据进行加载
    int[][] data=new int[4][4];
    int x=0;
    int y=0;

    //定义一个变量，记录当前展示的图片（方便更换图片）
    String path="image\\anime\\seaking\\";

    //定义一个二维数组，存储正确数据
    int[][] win={
            {1,2,3,4},
            {5,6,7,8},
            {9,10,11,12},
            {13,14,15,0}
    };

    //定义变量统计步数
    int step=0;


    //创建两个选项下面的条目对象
    //暂时无法实现    JMenuItem changeItem=new JMenuItem("更换图片");

    JMenuItem replayItem=new JMenuItem("重新游戏");
    JMenuItem reLoginItem=new JMenuItem("重新登录");
    JMenuItem closeItem=new JMenuItem("关闭游戏");

    JMenuItem man=new JMenuItem("人物");
    JMenuItem anime=new JMenuItem("动物");
    JMenuItem animal=new JMenuItem("动漫");

    JMenuItem accuntItem=new JMenuItem("公众号");


    JMenuItem rule=new JMenuItem("规则");

    public GameJFrame(){
        //初始化界面
        initJFrame();

        //初始化菜单
        initJMenuBar();

        //初始化数据（打乱）
        initData();

        //初始化图片
        initImage();

        //将窗体显示出来,一般写在最后面
        this.setVisible(true);
    }
    //初始化窗体
    public void initJFrame(){
        this.setSize(603,680);
        this.setTitle("拼图单机版 v1.0");
        //将窗体始终显示于最表层
        this.setAlwaysOnTop(true);
        //窗体居中
        this.setLocationRelativeTo(null);
        //设置关闭模式
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
        this.setLayout(null);
        //给整个界面添加键盘监听事件
        this.addKeyListener(this);
    }
    //初始化菜单
    public void initJMenuBar(){
        //创建整个菜单对象
        JMenuBar jMenuBar=new JMenuBar();

        //创建菜单上面的选项对象
        JMenu functionJMenu=new JMenu("功能");
        JMenu aboutJMenu=new JMenu("关于我们");
        JMenu gameRuleJMenu=new JMenu("游戏规则");

        JMenu checkImageJMenu=new JMenu("更换图片");

        //将条目添加到选项中
        functionJMenu.add(replayItem);
        functionJMenu.add(reLoginItem);
        functionJMenu.add(checkImageJMenu);
        functionJMenu.add(closeItem);

        checkImageJMenu.add(man);
        checkImageJMenu.add(animal);
        checkImageJMenu.add(anime);

        aboutJMenu.add(accuntItem);

        gameRuleJMenu.add(rule);

        //给条目绑定事件
        replayItem.addActionListener(this);
        reLoginItem.addActionListener(this);
        closeItem.addActionListener(this);
        accuntItem.addActionListener(this);
        rule.addActionListener(this);

        //将选项添加到菜单中
        jMenuBar.add(functionJMenu);
        jMenuBar.add(aboutJMenu);
        jMenuBar.add(gameRuleJMenu);

        //将菜单显示到界面上
        this.setJMenuBar(jMenuBar);

    }
    //初始化图片
    public void initImage(){
        //删除掉已经加载的图片
        this.getContentPane().removeAll();

        //显示胜利图标
        if(victory()){
            //显示胜利图标
            JLabel winJLabel=new JLabel(new ImageIcon("image\\yu.png"));
            winJLabel.setBounds(122,311,277,73);
            this.getContentPane().add(winJLabel);
        }

        JLabel gamename=new JLabel("拼图游戏");
        gamename.setForeground(Color.yellow);
        Font font0=new Font("微软雅黑", Font.BOLD,45);
        gamename.setFont(font0);
        gamename.setBounds(156,55,311,117);
        this.getContentPane().add(gamename);

        JLabel stepCount=new JLabel("步数"+step);
        stepCount.setForeground(Color.lightGray);
        Font font=new Font("微软雅黑", Font.PLAIN,15);
        stepCount.setFont(font);
        stepCount.setBounds(53,137,100,30);
        this.getContentPane().add(stepCount);

        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
              /*  //创建ImageIcon的对象
                ImageIcon icon=new ImageIcon("E:\\java\\PuzzleGame\\image\\animal\\1608e97bea585eeedf5c45d9450731d5.png");
                //创建JLabel的对象（管理容器）
                JLabel jLabel=new JLabel(icon);
                //把管理容器添加到界面
                this.getContentPane().add(jLabel);
               *////////////////////////////////////////////////////////////
                //获取当前要加载图片的序号
                int num=data[i][j];
                //创建JLabel的对象（管理容器）
                //相对路径
                ImageIcon image=new ImageIcon(path+num+".jpg");
                JLabel jLabel=new JLabel(image);
                //指定图片位置
                jLabel.setBounds(105*j+42,105*i+170,105,105);
                //给图片添加边框
                //LOWERED:让图片凹起
                //RAISED:让图片凸下去
                jLabel.setBorder(new BevelBorder(BevelBorder.RAISED));
                //把管理容器添加到界面
                this.getContentPane().add(jLabel);
            }
        }

        //添加背景图片
        JLabel background=new JLabel(new ImageIcon("image\\bj.png"));
        background.setBounds(0,0,608,680);
        //添加背景图片到界面中
        this.getContentPane().add(background);

        //注：先加载的图片会显示在最上方，后加载的会显示在下方

        //刷新一下界面
        this.getContentPane().repaint();
    }
    //初始化数据（打乱）
    public void initData(){
        //定义一个一维数组
        int[] tempArr={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};
        //打乱数组中的数据顺序
        //遍历数组，得到每一个元素，拿着每一个元素跟随机索引上面的数据进行交换
        Random r=new Random();
        for(int i=0;i<tempArr.length;i++){
            //提取随机索引
            int index=r.nextInt(tempArr.length);
            //拿着遍历到的每一个数据，跟随机索引上的数据进行交换
            int temp=tempArr[i];
            tempArr[i]=tempArr[index];
            tempArr[index]=temp;
        }

        //遍历一维数组中的元素，在添加到二维数组中
        for(int i=0;i<tempArr.length;i++){
            if(tempArr[i]==0){
                x=i/4;
                y=i%4;
            }
                data[i/4][i%4]=tempArr[i];
        }
    }

    //键入某键时
    @Override
    public void keyTyped(KeyEvent e) {

    }

    //按下某键时
    @Override
    public void keyPressed(KeyEvent e) {
        //判断游戏是否胜利，胜利则直接结束
        if(victory()){
            return;
        }

        int code=e.getKeyCode();
        if(code==65){
            //把整个界面全部删除
            this.getContentPane().removeAll();

            //加载第一张完整图片
            JLabel all=new JLabel(new ImageIcon(path+"all.jpg"));
            all.setBounds(42,170,420,420);
            this.getContentPane().add(all);

            //加载背景图片
            JLabel background=new JLabel(new ImageIcon("image\\bj.png"));
            background.setBounds(0,0,608,680);
            this.getContentPane().add(background);

            //刷新界面
            this.getContentPane().repaint();
        }
    }

    //松开某键时
    @Override
    public void keyReleased(KeyEvent e) {
        //判断游戏是否胜利，胜利则直接结束
        if(victory()){
            return;
        }

        //上：38 下：40 左：37 右:39
        int code=e.getKeyCode();
        if(code==37){
            if(y==3){
                return;
            }
            System.out.println("向左移动");
            data[x][y]=data[x][y+1];
            data[x][y+1]=0;
            y++;
            step++;
            initImage();

        }else if(code==38){
            if(x==3){
                return;
            }
            System.out.println("向上移动");
            data[x][y]=data[x+1][y];
            data[x+1][y]=0;
            x++;
            step++;
            initImage();
        }else if(code==39){
            if(y==0){
                return;
            }
            System.out.println("向右移动");
            data[x][y]=data[x][y-1];
            data[x][y-1]=0;
            y--;
            step++;
            initImage();
        }else if(code==40){
            if(x==0){
                return;
            }
            System.out.println("向下移动");
            data[x][y]=data[x-1][y];
            data[x-1][y]=0;
            x--;
            step++;
            initImage();
        }else if(code==65){
            initImage();
        }else if(code==87){
            data=new int[][]{
                    {1,2,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,0}
            };
            initImage();
        }
    }

    //判断data数组是否和win数组一样
    public boolean victory(){
        for(int i=0;i<data.length;i++){
            for(int j=0;j<data.length;j++){
                if(data[i][j]!=win[i][j]){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {


        //获取当前被点击的条目对象
        Object obj=e.getSource();
        //判断
        if(obj == replayItem){
            System.out.println("重新游戏");
            //计步器清零
            step=0;
            //再次打乱二维数组中的数据
            initData();
            //重新加载图片
            initImage();
        }else if(obj==reLoginItem){
            System.out.println("重新登录");
            //关闭当前登录界面
            this.setVisible(false);
            //打开登录界面
 ///////////////////////////////////////////////////////////////////////////
            //与loginJFrame的单例模式相配合
            LoginJFrame.getInstance().setVisible(true);
        }else if(obj==closeItem){
            System.out.println("关闭游戏");
            //直接关闭虚拟机
            System.exit(0);
        }else if(obj==accuntItem){
            System.out.println("公众号");

            //创建一个弹窗对象
            JDialog jDialog=new JDialog();

            JLabel stepCount=new JLabel("制作不易，打赏鼓励");
            stepCount.setForeground(Color.orange);
            Font font=new Font("微软雅黑", Font.BOLD,18);
            stepCount.setFont(font);
            stepCount.setBounds(6,0,199,30);
            jDialog.getContentPane().add(stepCount);

            //创建一个管理图片的容器对象
            JLabel jLabel=new JLabel(new ImageIcon("image\\money.png"));
            jLabel.setBackground(Color.red);
            //设置位置和宽高
            jLabel.setBounds(0,0,258,258);
            //把图片添加到弹框当中
            jDialog.getContentPane().add(jLabel);
            //给弹框设置大小
            jDialog.setSize(344,400);
            //让弹框置顶
            jDialog.setAlwaysOnTop(true);
            //让弹框居中
            jDialog.setLocationRelativeTo(null);
            //弹框不关闭则无法进行下面的界面
            jDialog.setModal(true);
            //让弹框显示出来
            jDialog.setVisible(true);
        }else if(obj==rule){
            //创建弹窗
            JDialog jDialog=new JDialog();
            jDialog.setTitle("游戏规则");
            //取消默认的居中放置，只有取消了才会按照XY轴的形式添加组件
            jDialog.setLayout(null);

            JLabel wrong=new JLabel( new ImageIcon("image\\rule.png"));

            wrong.setBounds(0,0,488,400);
            jDialog.getContentPane().add(wrong);

            jDialog.setSize(488,440);
            //让弹框置顶
            jDialog.setAlwaysOnTop(true);
            //让弹框居中
            jDialog.setLocationRelativeTo(null);
            //弹框不关闭则无法进行下面的界面
            jDialog.setModal(true);
            //让弹框显示出来
            jDialog.setVisible(true);
        }
    }
}
