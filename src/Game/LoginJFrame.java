package Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class LoginJFrame extends JFrame implements ActionListener{
    JButton login=new JButton("登录");
    JButton register=new JButton("注册");
    JPasswordField inputPassword=new JPasswordField();
    JTextField inputUsername=new JTextField();
    JTextField code=new JTextField();
    String rightcode=code();

    //定义集合存储用户信息

    ArrayList<User> list= RegisterJFrame.list0;

    private volatile static LoginJFrame instance;
    //登录界面
    public LoginJFrame(){
        //初始化界面
        initJFrame();
        //初始化内容
        initView();
        //初始化图片
        initImage();

        //将窗体显示出来，一般写在最后面
        this.setVisible(true);
    }

    //单例模式
    //对象使用之前通过getInstance得到而不需要自己定义，用完之后不需要delete
    public static LoginJFrame getInstance() {
        if (instance == null) {
            synchronized (LoginJFrame.class) {
                if (instance == null) {
                    instance = new LoginJFrame();
                }
            }
        }
        return instance;
    }

    private void initJFrame() {
        this.setSize(488,430);
        this.setTitle("用户登录");
        //将窗体始终显示于最表层
        this.setAlwaysOnTop(true);
        //窗体居中
        this.setLocationRelativeTo(null);
        //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
        this.setLayout(null);
        //设置关闭模式
        this.setDefaultCloseOperation(3);
    }

    private void initView() {
        JLabel gamename=new JLabel("拼图游戏");
        gamename.setForeground(Color.yellow);
        Font font0=new Font("微软雅黑", Font.BOLD,45);
        gamename.setFont(font0);
        gamename.setBounds(156,15,311,117);
        this.getContentPane().add(gamename);
        //用户名
        JLabel usernameText=new JLabel("用户名");
        usernameText.setForeground(Color.red);
        Font font=new Font("微软雅黑", Font.BOLD,15);
        usernameText.setFont(font);
        usernameText.setBounds(133,137,47,17);
        this.getContentPane().add(usernameText);


        inputUsername.setBounds(195,134,133,30);
        this.getContentPane().add(inputUsername);

        //密码
        JLabel passwordText=new JLabel("密码");
        passwordText.setForeground(Color.red);
        Font font1=new Font("微软雅黑", Font.BOLD,15);
        passwordText.setFont(font1);
        passwordText.setBounds(147,199,47,17);
        this.getContentPane().add(passwordText);


        inputPassword.setBounds(195,195,133,30);
        this.getContentPane().add(inputPassword);

        //验证码
        JLabel codeText=new JLabel("验证码");
        codeText.setForeground(Color.red);
        Font font2=new Font("微软雅黑", Font.BOLD,15);
        codeText.setFont(font2);
        codeText.setBounds(133,261,47,17);
        this.getContentPane().add(codeText);


        code.setBounds(195,255,60,30);
        this.getContentPane().add(code);

        //获取验证码
        JLabel code=new JLabel(rightcode);
        code.setForeground(Color.orange);
        code.setBounds(266,253,50,30);
        this.getContentPane().add(code);

        //设置登录按钮

        Font font3=new Font("微软雅黑", Font.PLAIN,16);
        login.setFont(font3);
        login.setBounds(155,315,66,30);
        this.getContentPane().add(login);

        //设置注册按钮

        Font font4=new Font("微软雅黑", Font.PLAIN,16);
        register.setFont(font4);
        register.setBounds(281,315,66,30);
        this.getContentPane().add(register);


        //将按钮绑定鼠标监听事件
        login.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                checkLogin(e);
            }
        });
        //this指当前类的所有方法
        register.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //关闭当前窗体
                qqq();
                new RegisterJFrame();
            }
        });
    }

    private void initImage() {
        //添加背景图片
        JLabel background=new JLabel(new ImageIcon("image\\R-C.png"));
        background.setBounds(0,0,488,430);
        //添加背景图片到界面中
        this.getContentPane().add(background);
    }

    public  String code(){
        ArrayList<Character> codelist=new ArrayList<>();
        for(int i=0;i<26;i++){
            codelist.add((char)('a'+i));
            codelist.add((char)('A'+i));
        }
        StringBuilder sb=new StringBuilder();
        Random random=new Random();
        for(int i=0;i<4;i++){
            int index=random.nextInt(codelist.size());
            char c=codelist.get(index);
            sb.append(c);
        }
        int num=random.nextInt(10);
        sb.append(num);
        char[] arr=sb.toString().toCharArray();
        int randomindex=random.nextInt(arr.length-1);
        //a.b||a->c,b->a,c->b
        char temp=arr[randomindex];
        arr[randomindex]=arr[arr.length-1];
        arr[arr.length-1]=temp;
        return new String(arr);
    }

    public void checkLogin(ActionEvent e){
        //获取输入的用户名和密码
        String inputname=inputUsername.getText();
        String inputword=inputPassword.getText();
        ///////////////////////////////////////////////
        List<Game.User> matchUserList = list.stream().filter(s -> s.getUsername().equals(inputname) && s.getPassword().equals(inputword)).toList();
        if (matchUserList.size() > 0) {
            checkcode();
        } else {
               //创建弹窗报错提醒
               JDialog jDialog=new JDialog();
               jDialog.setTitle("输入异常");
               //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
               jDialog.setLayout(null);

               JLabel wrong=new JLabel("用户名或密码输入错误，请重新登录");
               wrong.setForeground(Color.red);
               Font font=new Font("微软雅黑",0,13);
               wrong.setFont(font);
               wrong.setBounds(22,3,222,22);
              jDialog.getContentPane().add(wrong);

               jDialog.setSize(282,66);
               //让弹框置顶
               jDialog.setAlwaysOnTop(true);
               //让弹框居中
               jDialog.setLocationRelativeTo(null);
               //弹框不关闭则无法进行下面的界面
               jDialog.setModal(true);
               //让弹框显示出来
               jDialog.setVisible(true);
           }


    }

    public void checkcode(){
        String inputCode=code.getText();
        if(inputCode.equals(rightcode)){
            this.setVisible(false);
            new GameJFrame();
        }else{
            JDialog jDialog=new JDialog();
            jDialog.setTitle("输入异常");
            //取消默认居中(让窗体里面的所有组件都相对于一个坐标放置）
            jDialog.setLayout(null);

            JLabel jl=new JLabel("验证码输入错误");
            jl.setForeground(Color.red);
            Font font=new Font("微软雅黑",0,13);
            jl.setFont(font);
            jl.setBounds(22,3,222,22);
            jDialog.getContentPane().add(jl);

            jDialog.setSize(282,66);
            //让弹框置顶
            jDialog.setAlwaysOnTop(true);
            //让弹框居中
            jDialog.setLocationRelativeTo(null);
            //弹框不关闭则无法进行下面的界面
            jDialog.setModal(true);
            //让弹框显示出来
            jDialog.setVisible(true);
        }
    }

    public void qqq(){
        this.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

//    @Override
//    public void actionPerformed(ActionEvent e) {
//        //获取当前被点击的对象
//        Object obj1=e.getSource();
//        if(obj1==login){
//            System.out.println("登录");
//            //关闭当前登录界面
//            this.setVisible(false);
//            new GameJFrame();
//        }else if(obj1==register){
//            //关闭当前登录界面
//            this.setVisible(false);
//            check(list,inputUsername,inputPassword);
//        }
//    }
}
